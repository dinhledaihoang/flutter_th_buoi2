import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';

class login extends StatefulWidget {
  createState() {
    return LoginScreenState();
  }
}

class LoginScreenState extends State<login> {
  final formKey = GlobalKey<FormState>();

  Widget build(context) {
    return Container(
        margin: EdgeInsets.all(25.0), //25pixel
        child: Form(
          key: formKey,
          child: Column(
            children: [
              emailField(),
              firstNameField(),
              lastNameField(),
              dobField(),
              addressField(),
              submitBTN(),
            ],
          ),
        ));
  }

  Widget emailField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        labelText: 'Email Address',
        hintText: 'e.g. something@abc.xyz',
      ),
      validator: (value) {
        if (value != null) if (!value.contains('@')) {
          return "Your email is invalid";
        }
        return null;
      },
      onSaved: (value) {
        print(value);
      },
    );
  }

  Widget firstNameField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        labelText: 'First name',
        hintText: 'e.g. John',
      ),
      validator: (value) {
        if (value != null) if (value.length < 2) {
          return "Firstname is invalid";
        }
        return null;
      },
    );
  }

  Widget lastNameField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        labelText: 'Last name',
        hintText: 'e.g. Kenedy',
      ),
      validator: (value) {
        if (value != null) if (value.length < 2) {
          return "Lastname is invalid";
        }
        return null;
      },
    );
  }

  Widget dobField() {
    return TextFormField(
      keyboardType: TextInputType.datetime,
      decoration: InputDecoration(
        labelText: 'Enter year of birth',
        hintText: 'e.g. 2001',
      ),
      validator: (value) {
        if (value != null) if (value.length < 4 || int.parse(value) > 2022 && int.parse(value) is int) {
          return "Year of birth is invalid";
        }
        return null;
      },
    );
  }

  Widget addressField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        labelText: 'Your address',
        hintText: 'e.g. 197 Skylake, ABC Dist., XYZ city',
      ),
      validator: (value) {
        if (value != null) if (value.length < 10) {
          return "Address is invalid";
        }
        return null;
      },
    );
  }

  Widget submitBTN() {
    // ignore: deprecated_member_use
    return RaisedButton(
      color: Colors.blue,
      child: Text('SUBMIT'),
      onPressed: () {
        print(formKey.currentState?.validate());
      },
    );
  }
}
